package me.project.ews;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class PalFragment extends Fragment {

    NestedScrollView normalView, waspadaView, siagaView, awasView;
    ProgressBar progressBar;
    public PalFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pal, container, false);

        String urlCurrentState = "http://api.thingspeak.com/channels/199709/fields/1/last?key=D6YDI8JAZSEYIX90";

        normalView = view.findViewById(R.id.val_normal);
        waspadaView = view.    findViewById(R.id.val_waspada);
        siagaView = view.findViewById(R.id.val_siaga);
        awasView = view.findViewById(R.id.val_awas);
        progressBar = view.findViewById(R.id.progressBar);

        normalView.setVisibility(View.GONE);
        waspadaView.setVisibility(View.GONE);
        siagaView.setVisibility(View.GONE);
        awasView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(this.getActivity());
        String urlCurrentTempearture = "http://api.gesaang.com/api/channels/18/fields/field1/last?api_key=LL0SPRYLGPLPEMQSTKTCIIFVRA2B23";

        JsonObjectRequest jsonRequestCurrentState = new JsonObjectRequest(urlCurrentTempearture, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Double value = response.getDouble("value");

                            progressBar.setVisibility(View.GONE);

                            if(value<32){
                                normalView.setVisibility(View.VISIBLE);
                            }else if(value<37){
                                waspadaView.setVisibility(View.VISIBLE);
                            }else if(value<39){
                                siagaView.setVisibility(View.VISIBLE);
                            }else if(value>39){
                                awasView.setVisibility(View.VISIBLE);
                            }else{
                                waspadaView.setVisibility(View.VISIBLE);
                            }
                        }catch (JSONException error){
                            Log.d("ERROR",error.toString());
                            progressBar.setVisibility(View.GONE);
                            waspadaView.setVisibility(View.VISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(getActivity().findViewById(android.R.id.content),
                                "Network Error! Please try again!", Snackbar.LENGTH_LONG).show();
                        Log.d("ERROR",error.toString());
                    }
                }
        );

        queue.add(jsonRequestCurrentState);
        return view;
    }

}
