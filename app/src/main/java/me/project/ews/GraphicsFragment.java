package me.project.ews;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GraphicsFragment extends Fragment {
    LineChart chart;
    List<Entry> entries;
    List<Integer> colors;
    List<String> labels;
    Double highestValue, lowestValue, averageValue;
    TextView highestView, lowestView, averageView;


    public GraphicsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graphics, container, false);
        chart = view.findViewById(R.id.chart);
        highestView = view.findViewById(R.id.highestView);
        lowestView = view.findViewById(R.id.lowestView);
        averageView = view.findViewById(R.id.averageView);
        setGraph();
        highestValue = -1.0;
        lowestValue = -1.0;
        averageValue = -1.0;
        return view;
    }

    public void setGraph(){

        RequestQueue queue = Volley.newRequestQueue(this.getActivity());
        String url = "http://api.gesaang.com/api/channels/18/fields/field1?api_key=LL0SPRYLGPLPEMQSTKTCIIFVRA2B23";
        JsonObjectRequest request = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                        entries = new ArrayList<Entry>();
                        labels = new ArrayList<String>();
                        colors = new ArrayList<Integer>();
                        ArrayList<Entry> temperatureValues = new ArrayList<Entry>();
                        ArrayList<Entry> humidityValues = new ArrayList<Entry>();
                        LineDataSet dataSetTemperature, dataSetHumidity;

                        try{
                            JSONObject dataObject = response.getJSONObject("data");
                            JSONArray records = dataObject.getJSONArray("details");

                            Double total = 0.0;
                            for(int i=0; i<records.length(); i++){

                                Double value = Double.parseDouble(records.getJSONObject(i).getString("value"));
                                temperatureValues.add(new Entry(i, (float) value.floatValue()));
                                Log.d("VALUEEEE", ""+value);
                                total += value;
                                if(value > highestValue){
                                    highestValue = value;
                                }else if(value < lowestValue){
                                    lowestValue = value;
                                }else if(lowestValue == -1.00 || highestValue == -1.00){
                                    lowestValue = value;
                                    highestValue = value;
                                }
                            }
                            averageValue = total/records.length();

                        }catch(JSONException error){
                            Log.d("ERROR", error.toString());
                        }

                        dataSetTemperature = new LineDataSet(temperatureValues, "Temperature");
                        dataSetTemperature.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                        dataSetTemperature.setColor(Color.parseColor("#C3C60C"));
                        dataSetTemperature.setDrawCircleHole(false);

                        dataSets.add(dataSetTemperature);

                        //set Axis
                        chart.getAxisRight().setEnabled(false);
                        chart.getXAxis().setDrawGridLines(false);
                        YAxis yAxis = chart.getAxisLeft();
                        yAxis.setAxisMaxValue(100);
                        yAxis.setAxisMinValue(0);

                        LineDataSet lineDataSet = new LineDataSet(entries, "");
                        LineData dataSet = new LineData(dataSets);

                        //Chart Interaction Control
                        chart.setTouchEnabled(true);
                        chart.setDragEnabled(true);
                        chart.setScaleEnabled(true);
                        chart.setPinchZoom(true);

                        //set Dataset
                        chart.setData(dataSet);

                        //set Description
                        chart.getDescription().setText("Monitoring");

                        chart.animateXY(2000,2000);

                        chart.invalidate();

                        DecimalFormat df = new DecimalFormat(".##");
                        highestView.setText(df.format(highestValue)+" \u2103");
                        lowestView.setText(df.format(lowestValue)+" \u2103");
                        averageView.setText(df.format(averageValue)+" \u2103");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        );

        queue.add(request);
    }

}
