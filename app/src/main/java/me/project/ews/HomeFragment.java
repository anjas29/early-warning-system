package me.project.ews;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.lzyzsd.circleprogress.ArcProgress;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    TextView nameView, locationView;
    ArcProgress temperatureArcView;
    String nameString, addressString, latitudeString, longitudeString;
    double temperatureValue, humidityValue;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        nameView = view.findViewById(R.id.nameView);
        locationView = view.findViewById(R.id.locationView);
        temperatureArcView = view.findViewById(R.id.temperatureArcView);


        nameView.setText("Pos 1");
        locationView.setText("Kabupaten Sleman, Yogyakarta");

        RequestQueue queue = Volley.newRequestQueue(this.getActivity());

        String urlCurrentTempearture = "http://api.gesaang.com/api/channels/18/fields/field1/last?api_key=LL0SPRYLGPLPEMQSTKTCIIFVRA2B23";
        String urlCurrentHumidity = "http://api.gesaang.com/api/channels/18/fields/field2/last?api_key=LL0SPRYLGPLPEMQSTKTCIIFVRA2B23";

        JsonObjectRequest jsonRequestCurrentTemperature = new JsonObjectRequest(urlCurrentTempearture, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Double value = response.getDouble("value");
                            temperatureArcView.setProgress(value.intValue());
                        }catch (JSONException error){
                            Log.d("ERROR",error.toString());
                            temperatureArcView.setProgress(0);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ERROR",error.toString());
                    }
                }
        );

        queue.add(jsonRequestCurrentTemperature);

        return view;
    }
}
